package com.dotgreen.appdotgreen.models;

/**
 * Created by thalles on 01/05/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class PontosTroca {

    public final static String REFERENCE = "pontosTroca";

    private String id;
    private String nome;
    private double latitude;
    private double longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
