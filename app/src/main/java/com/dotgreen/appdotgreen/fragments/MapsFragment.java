package com.dotgreen.appdotgreen.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.dotgreen.appdotgreen.BuildConfig;
import com.dotgreen.appdotgreen.R;
import com.dotgreen.appdotgreen.activities.LoginActivity;
import com.dotgreen.appdotgreen.activities.MainActivity;
import com.dotgreen.appdotgreen.models.PontosTroca;
import com.dotgreen.appdotgreen.network.FirebaseConnection;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private boolean isLogged;
    private String mParam2;
    private GoogleMap map;

    private BottomSheetBehavior mBottomSheetBehavior;
    private ProgressDialog progressDialog;

    private OnFragmentInteractionListener mListener;
    private FirebaseConnection conn;
    private View bottomSheet;

    public MapsFragment() {
        // Required empty public constructor
    }

    public static MapsFragment newInstance(boolean param1, String param2) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isLogged = getArguments().getBoolean(ARG_PARAM1, false);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_maps, container, false);

        showProgressDialog();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        SupportMapFragment fragment = new SupportMapFragment();
        transaction.add(R.id.mapView, fragment);
        transaction.commit();

        fragment.getMapAsync(this);

        setBottomSheetBehavior(v);
        return v;
    }

    private void setBottomSheetBehavior(View v) {
        bottomSheet = v.findViewById(R.id.bottom_sheet);
        bottomSheet.setVisibility(View.VISIBLE);

        Button btnCadastrar = (Button) bottomSheet.findViewById(R.id.btn_cadastrar);
        Button btnEntrar = (Button) bottomSheet.findViewById(R.id.btn_entrar);
        ImageView imgCollapseBottomSheet = (ImageView) bottomSheet.findViewById(R.id.img_drag_down);

        imgCollapseBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateBottomSheet(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCadastro();
            }
        });

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });

        if (!isLogged) {
            mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBottomSheetBehavior.setPeekHeight(35);
        } else {
            bottomSheet.setVisibility(View.GONE);
        }
    }

    private void goToCadastro() {
        Intent intent = getActivity().getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MainActivity.BOTTOM_NAV_ITEM_SELECTED, MainActivity.ITEM_PERFIL);
        getActivity().finish();
        startActivity(intent);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (map != null)
            configMap();
    }

    private void configMap() {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings mapSettings = map.getUiSettings();

        mapSettings.setAllGesturesEnabled(true);
        mapSettings.setCompassEnabled(true);
        mapSettings.setZoomControlsEnabled(true);

        getPontosTroca();
    }

    private void getPontosTroca() {
        conn = new FirebaseConnection(getContext());
        conn.get(PontosTroca.REFERENCE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                marcarPontosNoMapa(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Toast.makeText(getActivity(), "Não foi possível conectar ao banco de dados!", Toast.LENGTH_SHORT).show();

                if (BuildConfig.BUILD_TYPE.equals("debug"))
                    Log.w("Erro", "Failed to read value.", error.toException());
            }
        });
    }

    private void marcarPontosNoMapa(DataSnapshot dataSnapshot) {
        List<PontosTroca> pontos = new ArrayList<>();
        for (DataSnapshot dss : dataSnapshot.getChildren()) {
            PontosTroca ponto = dss.getValue(PontosTroca.class);
            ponto.setId(dss.getKey());
            pontos.add(ponto);
        }

        dismissProgressDialog();

        LatLng local;

        for (PontosTroca ponto : pontos) {
            local = new LatLng(ponto.getLatitude(), ponto.getLongitude());
            map.addMarker(new MarkerOptions().position(local).title(ponto.getNome()));
            //map.animateCamera(CameraUpdateFactory.newLatLngZoom(local, 10.0f));
        }

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(pontos.get(0).getLatitude(), pontos.get(0).getLongitude()), 10.0f));
        //map.moveCamera(CameraUpdateFactory.newLatLng());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setStateBottomSheet(BottomSheetBehavior.STATE_EXPANDED);
            }
        }, 1200);
    }

    private void setStateBottomSheet(int stateBottomSheet) {
        if (mBottomSheetBehavior != null && bottomSheet.getVisibility() == View.VISIBLE)
            mBottomSheetBehavior.setState(stateBottomSheet);
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Conectando ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
