package com.dotgreen.appdotgreen.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dotgreen.appdotgreen.R;
import com.dotgreen.appdotgreen.activities.MainActivity;
import com.dotgreen.appdotgreen.activities.QRCodeScannerActivity;
import com.dotgreen.appdotgreen.utils.SharedPreferencesCustom;

@SuppressWarnings("FieldCanBeLocal")
public class QrCodeFragment extends Fragment {

    private ImageButton btnQrCode;

    public static QrCodeFragment newInstance() {
        return new QrCodeFragment();
    }

    public QrCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_qr_code, container, false);

        btnQrCode = (ImageButton) v.findViewById(R.id.btn_qrcode);
        btnQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                        showDialog("Permissão de câmera necessária!!!", true);
                    } else {
                        askForCameraPermission();
                    }
                    return;
                }
                callQrCodeScanner();
            }
        });
        return v;
    }

    private void showDialog(String msg, final boolean askForPermisson) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
        alertBuilder.setCancelable(false);
        alertBuilder.setMessage(msg);
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (askForPermisson)
                    askForCameraPermission();
            }
        });

        alertBuilder.show();
    }

    private void askForCameraPermission() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, MainActivity.MY_PERMISSIONS_REQUEST_CAMERA);
    }

    private void goToPoints() {
        Intent intent = getActivity().getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MainActivity.BOTTOM_NAV_ITEM_SELECTED, MainActivity.ITEM_POINTS);
        getActivity().finish();
        startActivity(intent);
    }

    public void callQrCodeScanner() {
        startActivityForResult(new Intent(getActivity(), QRCodeScannerActivity.class), MainActivity.READ_QRCODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String dados = "Nenhum dado escaneado!";
        if (data != null) {
            dados = data.getStringExtra(MainActivity.QRCODE);
            try {
                int pontos = Integer.valueOf(dados);
                pontos += SharedPreferencesCustom.read(getContext(), SharedPreferencesCustom.PONTOS, 0);
                SharedPreferencesCustom.save(getContext(), SharedPreferencesCustom.PONTOS, pontos);
                goToPoints();
            } catch (Exception ex) {
                ex.printStackTrace();
                showDialog("Erro ao escanear pontos!", false);
            }

            return;
        }

        showDialog(dados, false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MainActivity.MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callQrCodeScanner();
                } else {
                    Toast.makeText(getContext(), "Necessária permissão", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
