package com.dotgreen.appdotgreen.network;

import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by thall on 01/05/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class FirebaseConnection {

    private FirebaseApp firebaseApp;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    String value;

    public FirebaseConnection(Context context) {
       /* firebaseApp = FirebaseApp.getInstance();
        if (firebaseApp == null)
            FirebaseApp.initializeApp(context);*/

        this.database = FirebaseDatabase.getInstance();
    }

    public void post(String referencia) {
        myRef = database.getReference(referencia);
        myRef.setValue("Hello, ieie");
    }

    public DatabaseReference get(String referencia) {
        return database.getReference(referencia);
    }

    //Exemplo de como usar Conexão com Firebase
    /*  conn = new FirebaseConnection(this);
        conn.post("message");

        conn.get(PontosTroca.REFERENCE).addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            List<PontosTroca> pontos = new ArrayList<>();
            for (DataSnapshot dss: dataSnapshot.getChildren()) {
                PontosTroca ponto = dss.getValue(PontosTroca.class);
                ponto.setId(dss.getKey());
                pontos.add(ponto);
            }

            String listaPontos = "";
            for (PontosTroca ponto : pontos) {
                listaPontos +=
                        "Id: " + ponto.getId()
                                + "\n"
                                + "Nome: " + ponto.getNome()
                                + "\n"
                                + "Latitude: " + ponto.getLatitude()
                                + "\n"
                                + "Longitude: " + ponto.getLongitude()
                                + "\n\n";
            }
            mTextMessage.setText(
                    listaPontos
            );
        }

        @Override
        public void onCancelled(DatabaseError error) {
            Toast.makeText(MainActivity.this, "Não foi possível conectar ao banco de dados!", Toast.LENGTH_SHORT).show();
            Log.w("Erro", "Failed to read value.", error.toException());
        }
    });*/
}
