package com.dotgreen.appdotgreen.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dotgreen.appdotgreen.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnEntrar = (Button) findViewById(R.id.btn_entrar);
        Button btnFaceLogin = (Button) findViewById(R.id.btn_face_login);
        Button btnCadastrar = (Button) findViewById(R.id.btn_cadastrar);
        EditText editText = (EditText) findViewById(R.id.edt_email);
        TextView txtVerPontos = (TextView) findViewById(R.id.txt_ver_pontos);

        editText.requestFocus();
        btnEntrar.setOnClickListener(this);
        btnCadastrar.setOnClickListener(this);
        btnFaceLogin.setOnClickListener(this);
        txtVerPontos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.BOTTOM_NAV_ITEM_SELECTED, MainActivity.ITEM_MAPA);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
}
