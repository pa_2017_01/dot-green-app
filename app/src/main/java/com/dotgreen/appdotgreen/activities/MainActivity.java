package com.dotgreen.appdotgreen.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.dotgreen.appdotgreen.R;
import com.dotgreen.appdotgreen.fragments.MapsFragment;
import com.dotgreen.appdotgreen.fragments.PerfilFragment;
import com.dotgreen.appdotgreen.fragments.PointsFragment;
import com.dotgreen.appdotgreen.fragments.QrCodeFragment;
import com.dotgreen.appdotgreen.utils.SharedPreferencesCustom;

@SuppressWarnings("unused")
public class MainActivity extends AppCompatActivity implements PointsFragment.OnFragmentInteractionListener,
        PerfilFragment.OnFragmentInteractionListener, MapsFragment.OnFragmentInteractionListener {

    public static final String QRCODE = "QRCode";
    public static final String BOTTOM_NAV_ITEM_SELECTED = "item";
    public static final int READ_QRCODE = 1;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 1001;
    public static final int ITEM_POINTS = R.id.navigation_points;
    public static final int ITEM_QRCODE = R.id.navigation_qrcode;
    public static final int ITEM_MAPA = R.id.navigation_map;
    public static final int ITEM_PERFIL = R.id.navigation_profile;

    private int bottomNavItemSelected;
    private boolean isLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getParams();
        setupBottomNavBar();
    }

    private void getParams() {
        bottomNavItemSelected = getIntent().getIntExtra(BOTTOM_NAV_ITEM_SELECTED, ITEM_POINTS);
        isLogged = SharedPreferencesCustom.read(this, SharedPreferencesCustom.USER_ID, 0) > 0;
    }

    private void setupBottomNavBar() {
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(bottomNavItemSelected);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_points:
                    fragment = PointsFragment.newInstance("", "");
                    break;
                case R.id.navigation_qrcode:
                    fragment = QrCodeFragment.newInstance();
                    break;
                case R.id.navigation_map:
                    fragment = MapsFragment.newInstance(isLogged, "");
                    break;
                case R.id.navigation_profile:
                    fragment = PerfilFragment.newInstance("", "");
                    break;
            }

            fragmentTransaction(fragment);
            return true;
        }

    };

    private void fragmentTransaction(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
