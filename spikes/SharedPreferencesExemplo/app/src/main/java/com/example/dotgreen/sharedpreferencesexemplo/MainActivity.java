package com.example.dotgreen.sharedpreferencesexemplo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button limpar = (Button) findViewById(R.id.btn_limpar);
        limpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesCustom.clearPreference(MainActivity.this);
            }
        });

        final EditText edt = (EditText) findViewById(R.id.edit);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mensagem = edt.getText().toString();

                if (!TextUtils.isEmpty(mensagem)) {
                    SharedPreferencesCustom.save(MainActivity.this,
                            SharedPreferencesCustom.MENSAGEM,
                            mensagem
                    );
                }

                String mensagemMostrar = SharedPreferencesCustom.read(
                        MainActivity.this,
                        SharedPreferencesCustom.MENSAGEM,
                        ""
                );

                Snackbar.make(view, TextUtils.isEmpty(mensagemMostrar) ? "Nada no SharedPreferences" : mensagemMostrar, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                edt.getText().clear();
            }
        });
    }
}
