package com.example.thalles.mapsteste.extras;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.Random;

/**
 * Created by Thalles on 08/09/2016.
 */
public class Util {

    private static int generateRandomNumber(){
        Random random = new Random();
        return random.nextInt(5);
    }

    public static float getRandomMarkersColors(){
       int rand = generateRandomNumber();
       switch (rand) {
           case 0: return BitmapDescriptorFactory.HUE_ORANGE;
           case 1: return BitmapDescriptorFactory.HUE_BLUE;
           case 2: return BitmapDescriptorFactory.HUE_GREEN;
           case 3: return BitmapDescriptorFactory.HUE_RED;
           case 4: return BitmapDescriptorFactory.HUE_YELLOW;
           case 5: return BitmapDescriptorFactory.HUE_CYAN;
           default: return BitmapDescriptorFactory.HUE_RED;
       }
    }
}
