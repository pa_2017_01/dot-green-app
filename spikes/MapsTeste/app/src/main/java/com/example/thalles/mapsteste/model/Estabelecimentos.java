package com.example.thalles.mapsteste.model;

import com.example.thalles.mapsteste.entity.Estabelecimento;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Thalles on 08/09/2016.
 */
public class Estabelecimentos {

    private List<Estabelecimento> mEstabelecimentos = new ArrayList<>();

    public Estabelecimentos() {
        if (getTotal() == 0) {
            mEstabelecimentos.add(new Estabelecimento(1, "Indústria da Pizza", new LatLng(-19.97532514, -44.03527915), Collections.singletonList(("pizzaria")), "Melhor Pizzaria da região, venha experimentar nossas pizzas saborosas!!!"));
            mEstabelecimentos.add(new Estabelecimento(2, "ViaShopping", new LatLng(-19.9740748, -44.02142286), Collections.singletonList(("shopping")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(3, "Boi & Praia", new LatLng(-19.97197742, -44.03133631), Collections.singletonList(("espeto")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(4, "Lekinha's dog", new LatLng(-19.97621248, -44.03655589), Collections.singletonList(("cachorro-quente")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(5, "Lote's Bar", new LatLng(-19.9783703, -44.039855), Collections.singletonList(("bar")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(6, "Ligeirinho's Pizza", new LatLng(-19.97699898, -44.02514577), Collections.singletonList(("pizzaria, food truck")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(7, "Hot Dog", new LatLng(-19.97722081, -44.02557492), Collections.singletonList(("cachorro-quente")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(8, "Sexto Horário", new LatLng(-19.97845096, -44.02626157), Collections.singletonList(("espeto")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(9, "Low's Batata", new LatLng(-19.97839046, -44.02611136), Collections.singletonList(("batata")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(10, "Bacon Paradise", new LatLng(-19.97798713, -44.02536035), Collections.singletonList(("Hamburgueria")), "Descrição Breve do Local"));
            mEstabelecimentos.add(new Estabelecimento(11, "Espetos Espeteria", new LatLng(-19.97790647, -44.02413726), Collections.singletonList(("espeto")), "Descrição Breve do Local"));
        }
    }

    public List<Estabelecimento> getAll() {
        return mEstabelecimentos;
    }

    public int getTotal() {
        return mEstabelecimentos.size();
    }
}
