package com.example.thalles.mapsteste.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.thalles.mapsteste.R;
import com.example.thalles.mapsteste.entity.Estabelecimento;
import com.example.thalles.mapsteste.extras.DirectionsJSONParser;
import com.example.thalles.mapsteste.extras.Util;
import com.example.thalles.mapsteste.model.Estabelecimentos;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener {

    private final HashMap<Marker, Integer> mHashMap = new HashMap<>();
    List<Estabelecimento> mEstabelecimentos = new ArrayList<>();
    private static final String TAG = "CONNECTION";
    private boolean mIsLocationUpdatesEnabled;
    private boolean mMoveCameraToMyPlace;
    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mMarker;
    private LatLng mPositionMarkerClicked;
    private LatLng mMyPosition;
    private TextView mTextView;
    private Polyline mPolyline;
    private int mIdMarker = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        mIsLocationUpdatesEnabled = false;
        mTextView = (TextView) findViewById(R.id.textView);
        //FirebaseCrash.report(new Exception("My first Android non-fatal error"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        mMoveCameraToMyPlace = true;

        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }

        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);

            mapFragment.getMapAsync(this);
        }

        if (!mIsLocationUpdatesEnabled && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            mIsLocationUpdatesEnabled = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && mIsLocationUpdatesEnabled) {
            stopLocationUpdates();
            mIsLocationUpdatesEnabled = false;
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            finish();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    private double calcDistance(LatLng myPosition, LatLng selectedPosition, boolean show) {
        double distance = SphericalUtil.computeDistanceBetween(myPosition, selectedPosition);
        if (show)
            mTextView.setText("Estabelecimento está a " + formatNumber(distance) + " de distância");

        return distance;
    }

    private void updatePolyline(LatLng myPosition, LatLng selectedPosition) {
        //mPolyline.setPoints(Arrays.asList(myPosition, selectedPosition));
    }

    private String formatNumber(double distance) {
        String unit = "m";
        if (distance < 1) {
            distance *= 1000;
            unit = "mm";
        } else if (distance > 1000) {
            distance /= 1000;
            unit = "km";
        }

        return String.format("%4.3f%s", distance, unit);
    }

    @Override
    public void onMapReady(GoogleMap retMap) {
        mMap = retMap;
        configMap();
    }

    public void configMap() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMarkerClickListener(this);

       /* PolylineOptions options = new PolylineOptions();
        options.color(Color.parseColor("#CC0000FF"));
        options.width(5);

        mPolyline = mMap.addPolyline(options);*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            finish();
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    protected synchronized void buildGoogleApiClient() {
        //Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(this, "onConnected", Toast.LENGTH_SHORT).show();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        //mLocationRequest.setSmallestDisplacement(0.1F);

        if (!mIsLocationUpdatesEnabled) {
            startLocationUpdates();
            mIsLocationUpdatesEnabled = true;
        }

        if (!mHashMap.isEmpty()) {
            mHashMap.clear();
            mIdMarker = 0;
        }

        Estabelecimentos estabelecimentos = new Estabelecimentos();
        if (!mEstabelecimentos.isEmpty()) {
            mEstabelecimentos.clear();
        }
        mEstabelecimentos = estabelecimentos.getAll();

        for (Estabelecimento local : mEstabelecimentos) {
            addMarkers(local.getCoordenadas(),
                    local.getRazaoSocial(),
                    local.getTag().get(0));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        //remove previous current location Marker
        /*if (marker != null) {
            marker.remove();
        }*/

        double dLatitude = mLastLocation.getLatitude();
        double dLongitude = mLastLocation.getLongitude();

        mMyPosition = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

        Log.i("LOCATION_CHANGED", dLatitude + " / " + dLongitude);
        /*marker = mMap.addMarker(new MarkerOptions().position(new LatLng(dLatitude, dLongitude))
                .title("My Location").icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)).draggable(true));*/
        if (mMoveCameraToMyPlace) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 14.0f));
            mMoveCameraToMyPlace = false;
        }
    }

    private void addMarkers(LatLng coordenadas, String razaoSocial, String tipo) {
        Marker marker = mMap.addMarker(new MarkerOptions().position(coordenadas)
                .title(razaoSocial)
                .snippet(tipo)
                .icon(BitmapDescriptorFactory.defaultMarker(Util.getRandomMarkersColors())));

        identifyEachMarker(marker);
    }

    private void identifyEachMarker(Marker currentMarker) {
        mHashMap.put(currentMarker, mIdMarker);
        mIdMarker++;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int pos = mHashMap.get(marker);
        Log.i("POSICAO x ID", String.valueOf(pos));
        mPositionMarkerClicked = marker.getPosition();
        double distancia = calcDistance(mMyPosition, mPositionMarkerClicked, false);
        showInformationsDialog(pos, distancia);
        return false;
    }

    private void showInformationsDialog(int pos, double distancia) {
        Estabelecimento local = mEstabelecimentos.get(pos);
        new MaterialStyledDialog(this)
                .setHeaderDrawable(ContextCompat.getDrawable(this, R.drawable.header_bkg))
                .setTitle(local.getRazaoSocial())
                .setCancelable(true)
                .setDescription(setDescription(local.getTag().get(0), local.getDescricao(), distancia))
                //.setStyle(Style.HEADER_WITH_ICON)
                //.setIcon(ContextCompat.getDrawable(this, R.mipmap.ic_launcher))
                .setNegative("Cancelar", new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .setPositive("Ir Para", new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //updatePolyline(mMyPosition, mPositionMarkerClicked);
                        calcDistance(mMyPosition, mPositionMarkerClicked, true);

                        // Getting URL to the Google Directions API
                        String url = getDirectionsUrl(mMyPosition, mPositionMarkerClicked);

                        DownloadTask downloadTask = new DownloadTask();

                        // Start downloading json data from Google Directions API
                        downloadTask.execute(url);
                    }
                })
                .setStyle(Style.HEADER_WITH_TITLE)
                .withDarkerOverlay(true)
                .show();
    }

    private String setDescription(String especialidade, String descricao, double distancia) {
        return "Especialidade: " + especialidade
                + "\nDescrição: " + descricao
                + "\nDistância do local: " + formatNumber(distancia);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        /*
        for(int i=2;i<markerPoints.size();i++){
            LatLng point  = (LatLng) markerPoints.get(i);
            if(i==2)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }*/

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Ex downloading url", e.toString());
        } finally {
            assert iStream != null;
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service

            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            if (mPolyline != null) {
                mPolyline.remove();
            }

            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);
            }
            // Drawing polyline in the Google Map for the i-th route
            mPolyline = mMap.addPolyline(lineOptions);
        }
    }
}
