package com.example.thalles.mapsteste.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.thalles.mapsteste.R;


public class ChooseLoginTypeActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_login_type);

        findViewById(R.id.btn_choose_comerciante).setOnClickListener(this);
        findViewById(R.id.btn_choose_user).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_choose_comerciante:
                //startActivity(new Intent(ChooseLoginTypeActivity.this, LoginComercianteActivity.class));
                break;

            case R.id.btn_choose_user:
                startActivity(new Intent(ChooseLoginTypeActivity.this, UserLoginActivity.class));
        }
    }
}
