package com.example.thalles.mapsteste.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Thalles on 08/09/2016.
 */
public class Estabelecimento {

    private int id;
    private String razaoSocial;
    private LatLng coordenadas;
    private List<String> tag;
    private String descricao;

    public Estabelecimento(int id, String razaoSocial, LatLng coordenadas, List<String> tag, String descricao) {
        this.id = id;
        this.razaoSocial = razaoSocial;
        this.coordenadas = coordenadas;
        this.tag = tag;
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public LatLng getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(LatLng coordenadas) {
        this.coordenadas = coordenadas;
    }

    public List<String> getTag() {
        return tag;
    }

    public void setTag(List<String> tag) {
        this.tag = tag;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
