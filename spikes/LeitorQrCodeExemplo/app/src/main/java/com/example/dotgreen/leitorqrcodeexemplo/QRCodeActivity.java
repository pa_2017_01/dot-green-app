package com.example.dotgreen.leitorqrcodeexemplo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by thalles on 01/05/16.
 */
@SuppressWarnings("DefaultFileTemplate")
public class QRCodeActivity extends Activity implements ZXingScannerView.ResultHandler{

    private final static String TAG = "QRCode: ";
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        changeSnackbarTextColor(
                Snackbar.make(mScannerView, "Escaneie o QRCode", Snackbar.LENGTH_INDEFINITE),
                Color.YELLOW).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        Log.i(TAG, result.getText()); // Prints scan results
        Log.i(TAG, result.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        setResult(RESULT_OK, new Intent().putExtra(MainActivity.QRCODE, result.getText()));
        finish();

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }

    private Snackbar changeSnackbarTextColor(Snackbar snackbar, int color){
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(color);

        return snackbar;
    }
}
